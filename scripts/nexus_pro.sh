#!/bin/bash

if [ "$#" -ne 1 ]; then
    echo "Usage: $0 <ssh_key>"
    exit 1
fi

SSH_KEY=$1

# Provision ubuntu server from digital ocean
doctl compute droplet create \
    --image ubuntu-23-10-x64 \
    --size s-4vcpu-8gb \
    --region lon1 \
    --vpc-uuid 1283f4ac-ec13-43f4-ad1f-d880ede8b69a \
    --ssh-keys $SSH_KEY \
    nexus-server

# Wait for server to be fully provisioned
sleep 60

# Transfer startup scripts to server
scp -o StrictHostKeyChecking=no startup_nexus.sh create.sh nexus_startup.sh run.sh root@$(doctl compute droplet list -o json | jq -r '.[] | select(.name == "nexus-server") | .networks.v4[] | select(.type == "public") | .ip_address'):/opt/

# Start nexus startup script
ssh -o StrictHostKeyChecking=no root@$(doctl compute droplet list -o json | jq -r '.[] | select(.name == "nexus-server") | .networks.v4[] | select(.type == "public") | .ip_address') "cd /opt/; ./nexus_startup.sh"
