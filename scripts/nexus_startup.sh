#!/bin/bash

echo "Installing Nexus and dependecies"
apt update -y && apt install openjdk-8-jre-headless net-tools -y \
    && apt install nodejs npm -y
wget https://download.sonatype.com/nexus/3/nexus-3.67.1-01-java8-unix.tar.gz

tar -zvxf nexus-3.67.1-01-java8-unix.tar.gz

echo "Creating nexus user"
adduser --disabled-password --gecos "" nexus
chown -R nexus:nexus nexus-3.67.1-01
chown -R nexus:nexus sonatype-work

echo run_as_user="nexus" > nexus-3.67.1-01/bin/nexus.rc

echo "Starting nexus service as nexus user"
su -c "/opt/nexus-3.67.1-01/bin/nexus start" - nexus
sleep 10
netstat -lntp
